# Python Development Environment

Answer to the Exercises on [Issue #34](https://gitlab.com/DareData-open-source/tutorials/-/issues/34#exercises). The goal is to organize properly the pipeline files into directories according to the problem presented. The code does not actully work, this is a theoretical project.

## How to use

- `bin/` contains the main file.
- `data_loader/` is the package responsible for loading the data.
- `data_preprocessing/` is the package that takes care of the data preprocessing: data cleaning and outlier detection.
- `data_visualization/` is the package that takes care of the data analysis and reports generation.

## Requirements

Execute `requirements.txt` with `pip install -r requirements.txt`, but since this repo is more conceptually focused, there is no need to execute. The purpose of this file in this context is just to keep the organization.
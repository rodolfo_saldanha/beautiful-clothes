import data_loader.db_loader as db_loader
import data_preprocessing.data_cleaning as data_cleaning
import data_preprocessing.outlier_detection as outlier_detection
import data_visualization.data_analysis as data_analysis


if __name__ == "__main__":
    conn = db_loader.db_connection()
    raw_data = db_loader.load_data()
    tmp_data = data_cleaning.drop_na(raw_data)
    outlier_detection.outlier_analysis(tmp_data)
    preprocessed_data = outlier_detection.outlier_removal(tmp_data)
    data_analysis.generate_reports(preprocessed_data)

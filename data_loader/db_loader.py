def db_connection():
    '''
    Connects to the database.
    '''
    return connection


def load_data():
    '''
    Loads data from database.
    '''
    return raw_data


if __name__ == "__main__":
    conn = db_connection()
    raw_data = load_data()
